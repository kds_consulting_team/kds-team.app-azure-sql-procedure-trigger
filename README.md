# Microsoft SQL Procedure Trigger

Application capable of executing stored procedures in Microsoft SQL Server databases. (Synapse, SQL Server, Azure, etc.)

## Configuration

1. On the component level, fill in the credentials for the SQL server where you want to trigger the procedure.
2. Add one configuration for each procedure you want to trigger. You can pass multiple arguments by specifying the name and value.

## Development
 
Clone this repository and init the workspace with following command:

```sh
git clone https://github.com/keboola/synapse-transformation
cd synapse-transformation
docker-compose build
docker-compose run --rm dev composer install --no-scripts
```

Create `.env` file with following contents
```env
SYNAPSE_SERVER=
SYNAPSE_PORT=
SYNAPSE_DATABASE=
SYNAPSE_UID=
SYNAPSE_PWD=
```

Run the test suite using this command:

```sh
docker-compose run --rm dev composer tests
```
 
# Integration

For information about deployment and integration with KBC, please refer to the [deployment section of developers documentation](https://developers.keboola.com/extend/component/deployment/) 
