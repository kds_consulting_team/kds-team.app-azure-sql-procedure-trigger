- In the credential settings below, fill in the credentials for the Microsoft SQL Server where you want to trigger the procedure.


- Add one configuration for each procedure you want to trigger. You can pass multiple arguments by specifying the name and value.