<?php

declare(strict_types=1);

namespace Keboola\SynapseTrigger;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\DBALException;
use Keboola\SynapseTrigger\Configuration\Code;
use Keboola\SynapseTrigger\Configuration\Script;
use Keboola\SynapseTrigger\Configuration\Block;
use Keboola\SynapseTrigger\Exception\UserException;
use Psr\Log\LoggerInterface;

class QueryRunner
{
    private LoggerInterface $logger;

    private Connection $connection;

    public function __construct(LoggerInterface $logger, Connection $connection)
    {
        $this->logger = $logger;
        $this->connection = $connection;
    }

    public function triggerProcedure(string $procedureName, array $procedureParameters): void
    {

        $query = $this->buildExecQuery($procedureName, $procedureParameters);

        // Run
        $this->logger->info(sprintf('Running query "%s".', $query));
        try {
            $this->connection->exec($query);
        } catch (\Throwable $originException) {
            $exception = $originException;

            // Unwrap to get better error message
            if ($exception instanceof DBALException) {
                $exception = $exception->getPrevious() ?? $exception;
            }

            // Remove driver prefix to get simpler message
            $message = preg_replace('~^SQLSTATE\[.+\[SQL Server\]~i', '', $exception->getMessage());

            throw new UserException(sprintf(
                'Query "%s" failed: "%s"',
                $query,
                $message,
            ), 0, $originException);
        }
    }

    private function buildExecQuery(string $procedureName, array $parameters): string
    {
        $this->validateArgument($procedureName);
        $argumentList = [];
        $query = 'EXEC ' . $procedureName;
        foreach ($parameters as $variable) {
            $this->validateArgument($variable['name']);
            $this->validateArgument($variable['value']);
            $argumentList[] = '@' . $variable['name'] . ' = ' . $variable['value'];
        }
        $arguments = implode(',', $argumentList);
        $query .= ' ' . $arguments . ';';
        return $query;
    }

    private function validateArgument(string $argument): bool
    {
        $invalidCharacters = array(' ', ';');
        foreach ($invalidCharacters as $character) {
            if (strpos($argument, $character) !== false) {
                throw new UserException(sprintf(
                    'Invalid argument "%s""',
                    $argument,
                ), 0);
            }
        }
        return true;
    }
}
