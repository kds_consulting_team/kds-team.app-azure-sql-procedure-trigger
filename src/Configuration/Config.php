<?php

declare(strict_types=1);

namespace Keboola\SynapseTrigger\Configuration;

use Keboola\Component\Config\BaseConfig;

class Config extends BaseConfig
{
    public const DEFAULT_QUERY_TIMEOUT = 7200;

    public function getHost(): string
    {
        return $this->getValue(['parameters', 'server']);
    }

    public function getPort(): int
    {
        return $this->getValue(['parameters', 'port']);
    }

    public function getUser(): string
    {
        return $this->getValue(['parameters', 'user_name']);
    }

    public function getPassword(): string
    {
        return $this->getValue(['parameters', '#password']);
    }

    public function getDatabase(): string
    {
        return $this->getValue(['parameters', 'database']);
    }

    public function getProcedureName(): string
    {
        return $this->getValue(['parameters', 'name']);
    }

    public function getProcedureParameters(): array
    {
        return $this->getValue(['parameters', 'procedure_parameters']);
    }


}