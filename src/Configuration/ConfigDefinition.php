<?php

declare(strict_types=1);

namespace Keboola\SynapseTrigger\Configuration;

use Keboola\Component\Config\BaseConfigDefinition;
use Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition;

class ConfigDefinition extends BaseConfigDefinition
{


    protected function getParametersDefinition(): ArrayNodeDefinition
    {
        $parametersNode = parent::getParametersDefinition();

        // @formatter:off
        $parametersNode
            ->isRequired()
            ->children()
            ->scalarNode('server')
            ->isRequired()
            ->end()
            ->integerNode('port')
            ->defaultValue(1433)
            ->end()
            ->scalarNode('user_name')
            ->isRequired()
            ->end()
            ->scalarNode('#password')
            ->isRequired()
            ->end()
            ->scalarNode('database')
            ->isRequired()
            ->end()
            ->scalarNode('name')
            ->isRequired()
            ->end()
            ->arrayNode('procedure_parameters')->variablePrototype()
            ->end()
            ->end();

        // @formatter:on

        return $parametersNode;
    }
}
