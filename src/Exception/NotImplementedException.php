<?php

declare(strict_types=1);

namespace Keboola\SynapseTrigger\Exception;

use Keboola\CommonExceptions\ApplicationExceptionInterface;

class NotImplementedException extends \Exception implements ApplicationExceptionInterface
{

}
