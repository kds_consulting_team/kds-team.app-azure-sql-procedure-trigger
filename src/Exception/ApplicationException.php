<?php

declare(strict_types=1);

namespace Keboola\SynapseTrigger\Exception;

use Keboola\CommonExceptions\ApplicationExceptionInterface;

class ApplicationException extends \Exception implements ApplicationExceptionInterface
{

}
