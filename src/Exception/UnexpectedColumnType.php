<?php

declare(strict_types=1);

namespace Keboola\SynapseTrigger\Exception;

use Keboola\CommonExceptions\ApplicationExceptionInterface;

class UnexpectedColumnType extends \Exception implements ApplicationExceptionInterface
{

}
