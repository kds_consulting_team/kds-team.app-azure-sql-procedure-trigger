<?php

declare(strict_types=1);

namespace Keboola\SynapseTrigger;

use Psr\Log\LoggerInterface;
use Keboola\Component\BaseComponent;
use Keboola\SynapseTrigger\Configuration\Config;
use Keboola\SynapseTrigger\Configuration\ConfigDefinition;
use Keboola\SynapseTrigger\Platform\ConnectionFactory;

class Component extends BaseComponent
{
    private QueryRunner $queryRunner;

    private LoggerInterface $logger;

    public function __construct(LoggerInterface $logger)
    {
        parent::__construct($logger);
        $connectionFactory = new ConnectionFactory();
        $connection = $connectionFactory->createFromConfig($this->getConfig());
        $this->queryRunner = new QueryRunner($logger, $connection);
        $this->logger = $logger;
    }


    protected function run(): void
    {
        $config = $this->getConfig();
        $this->queryRunner->triggerProcedure($config->getProcedureName(), $config->getProcedureParameters());
        $this->logger->info('Finished');
    }


    public function getConfig(): Config
    {
        $config = parent::getConfig();
        assert($config instanceof Config);
        return $config;
    }

    protected function getConfigClass(): string
    {
        return Config::class;
    }

    protected function getConfigDefinitionClass(): string
    {
        return ConfigDefinition::class;
    }
}
